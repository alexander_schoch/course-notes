---
title: Seminar Notes - Philosophical Aspects of Quantum Physics
author: Alexander Schoch
date: Thursday, September 20
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents

# Schedule

- Critical Text: Wed, 23:55 after the first session of the topic
- 2 questions/comments: Tue, 23:55 after the second session of the topic
