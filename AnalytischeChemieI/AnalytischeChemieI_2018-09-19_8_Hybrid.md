---
title: Hybrid Notes - Analytische Chemie I
author: Alexander Schoch
date: Wednesday, September 19
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents

# Lesen von NMR-Spektren

## Beispiel: 

### 13C-NMR

- Anzahl peaks = anzahl C-atome (die eine unterschiedliche umgebung haben)
- Gleiche Chemische Umgebung $\rightarrow$ gleicher Peak. "Gleiche Umgebung" = gleiche Nachbarn, gleiche geometrie, etc. typisch bei Para-Bezolringen

### Massenspektrum

- Höchte Masse = gesamtmasse
- Es können im vornherein mögliche Summenformeln gesammelt werden
- Einige Summenformeln können nun ausgeschlossen werden, da die anzahl an C-atomen nicht stimmt
    - Die anzahl Peaks aus dem NMR-Spektrum zeigt die Mindestanzahl an C-Atomen
- Mittels ausschlussverfahren können am Ende nun konstitutionsisomere ausgeschlossen werden
- Tiefere Peaks sind fragmente. können zum rückschluss genutzt werden.

### DEPT135

- Alle peaks, die nach unten zeigen, sind CH2
- Alle peaks, die nach oben zeigen, sind CH oder CH3

### DEPT90

- Alle peaks sind CH

