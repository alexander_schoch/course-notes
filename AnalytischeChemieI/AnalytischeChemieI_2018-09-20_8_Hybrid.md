---
title: Hybrid Notes - Analytische Chemie I
author: Alexander Schoch
date: Thursday, September 20
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents

# Übungen

- Do/Fri via Moodle
- ca. 25min Aufwand
