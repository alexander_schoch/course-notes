---
title: Seminar Notes - Die Rolle des Geistigen Eigentums im Ingenieurwesen und den technischen Wissenschaften
author: Alexander Schoch
date: Friday, September 21
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents


