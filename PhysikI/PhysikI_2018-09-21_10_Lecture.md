---
title: Lecture Notes - Physik I
author: Alexander Schoch
date: Friday, September 21
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents

# Wesen der Physik

- Experiment: Ausschluss möglichst vieler Faktoren, um möglichst eindeutige Resultate zu erhalten.
- Theorie: Es gibt keine absoulut richtige Theorie, sondern nur Gute, die auf viele Situationen passen

# Physikalische Grössen

- ist ein Produkt aus Zahlenwert und Einheit: $G = \{G\} * [G]$
- Beispiele:
    - Alter des Universums $= 14 * 10^9 a$
    - Zeit zwischen 2 Herzschlägen $\approx 1s$

## Grundeinheiten der Mechanik

- Länge $l: [l]=$m
- Zeit $t: [t]=$s
- Masse $m: [m]=$kg, die Masse eines Körpers ist überall konstant, das Gewicht aber nicht.

## Messungenauigkeiten

- Experiment: Kanone schiesst 1m, Zeitintervall: 3.9 ms $\rightarrow$ 923 km/h
- Messungenauigkeit:
    - $\Delta x = ( 1 \pm 0.01) m$
    - $\Delta t = (3.9 \pm 0.1) ms$
- $v_{gemessen} = [v_{min}, v_{max}]$ 
- extremwerte einsetzen (mit messungenauigkeiten)

