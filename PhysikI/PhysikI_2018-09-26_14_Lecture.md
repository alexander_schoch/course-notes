---
title: Lecture Notes - Physik I
author: Alexander Schoch
date: Wednesday, September 26
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents

# Radioaktiver Zerfall

- Zuviele Protonen $\rightarrow$ Elektromagnetsiche Kraft > Kernkraft $\rightarrow$ ein Proton wandelt sich in ein Neutron, ein Positron und ein Elektronenneutrino um $\rightarrow$ $\beta^+$-Zerfall
    - Allgemein:
$$
X \rightarrow Z + S
$$
    - Einstein: spez. rel. Theorie: Energie eines ruhenden Systems mit masse m: $E = mc^2$
    - Energieerhaltung: totale Energie vorher = totale Energie nachher
$$
E_x = E_y + E_s + Q_B
$$
        - $E_i$ = Ruheenergie von x,y,z, $Q_B$ = evt. frei werdende zusätzliche Energie
$$
m_xc^2 = m_yc^2 + m_sc^2 + Q_B
$$
        - $\rightarrow$ keine Massenerhaltung, weil $Q_B \neq 0$, Massendefekt
  - Beispiel:
$$
p \rightarrow n + e^+ + \nu_e
$$
    - Gesamtladung und Massenzahl bleiben konstant.
    - Tatsächlich wandelt ein up-quark sich in ein down-quark und $W^+$ um. $W^+$ wandelt sich in $e^+$ und $\nu_e$ um.
- Annihilation:
$$ 
e^+ + e^- \rightarrow \gamma + \gamma
$$
    - Annahme: Positronen und Elektronen sind ungefähr in Ruhe
    - Es gild: Energie- und Impulserhaltung
    - Energie vorher: 
$$
E_{e^+} + E_{e^-} = m_{e^+}c^2 + m_{e^-}c^2  = 2m_{e^-}c^2
$$
    - Energie nachher:
$$
E_{\gamma_1} + E_{\gamma_2}
$$
    - Impulserhaltung: Impuls vorher = $\vec{0}$ = Impuls nachher
$$
\vec{p_{\gamma_1}} = - \vec{p_{\gamma_2}} \Rightarrow |\vec{p_{\gamma_1}}| = |\vec{p_{\gamma_2}} |
$$
    - Es gild für Photonen: $E_{\gamma} = c|\vec{p_{\gamma}}|$
$$
E_{\gamma_1} = m_{e^-}c^2
$$
    - $E_{\gamma_1} = E_{\gamma_2} = 551 MeV \approx 0.5 GeV$, deswegen "gamma"-strahlung
    - Beobachtung: Exp. Abnahme der Anzahl Photonenpäre pro Zeiteinheit
    - Radioaktiver Zerfall: Zufallsprozess
