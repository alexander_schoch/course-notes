---
title: Lecture Notes - Mathematics III - Partial Differential Equations
author: Alexander Schoch
date: Thursday, September 20
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents

# Beispiele

- Wellengleichung: Beschreibt die Ausreitung in einer Flüssigkeit oder Auslenkung
- Wärmeleitungsgleichung: Ausbreitung von Wärme
- Laplace-Gleichung / Potentialgleichung: Elektrische Felder / Graviationsfelder. Wie bewegt sich ein Körper?

# Begriffe

- PDE: Parital Differential Equations, 
- ODE: Ordinary Differential Equation

ODEs sind Gleichungen für eine unbekannte Funktion in einer unabhängingen Variablen, wobei darin mindestens eine der Ableitungen der Funktion vorkommt

Eine PDE sind Gleichungen für eine gseuchte Fkt von mehreren unabhäningen Variablen, in der partielle Ableitungen dieser Fkt vorkommen.

## Beispiel ODE

- 2. Newton'sches gesetz
$$
m * \frac{d^2x(t)}{dt^2} = F(x(t))
$$
 
- Unbekannte Fkt $x(t) = (x_1(t),x_2(t),x_3(t)$), Position zur Zeit t
- $\frac{dx(t)}{dt}$ ist der Geschwindigkeitsvektor
- $\frac{d^2x(t)}{dt^2}$ ist die Beschlueunigung
- $m$ = Masse (konst)
- F(x) = Kraftfeld

## Beispiel PDE

- Wellengleichung (für eine Seite)

$$
\frac{\delta ^2u}{\delta t^2} = c^2 * \frac{\delta ^2u}{\delta x^2}
$$

- Unbekannte Fkt u(x,t) stellt die Auslenkung des Seitenpunktes mit Koordinate x zur Zeit t dar. 
- $u(x,0) =$ Anfangsbedingung (IC = initial condition)

- $u(0,t) = u(L,t) = 0$ für alle t.

### Wellengleichug für eine "ideale" Saite, siehe Notizheft
