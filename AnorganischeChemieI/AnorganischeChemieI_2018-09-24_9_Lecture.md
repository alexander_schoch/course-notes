---
title: Lecture Notes - Anorganische Chemie I
author: Alexander Schoch
date: Monday, September 24
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle
\tableofcontents


